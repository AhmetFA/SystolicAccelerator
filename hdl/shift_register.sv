//-------------------------------------------------------------------------------
//--Project: SystolicAccelerator
//--File: interfaces.sv
//--Date: 10.08.2022
//--Engineer : Ahmet Fatih ANKARALI
//--Description:
//-------------------------------------------------------------------------------

import systolic_pkg::*;
interface shift_register_interface 
    (
    input logic clock_i,               //system clock
    input logic resetn_i               //system active low reset
    );
    
    t_mac_data data_i;
    t_mac_data data_o;

    modport rtl(
        input data_i,
        output data_o
    );
endinterface

module shift_register #( parameter int length_c = 1 )( shift_register_interface.rtl shift_register_if);
    t_mac_data register_s [length_c];

    assign shift_register_if.data_o = register_s[length_c-1];
    
    genvar i;
    generate
        if (length_c > 1) begin
            for ( i = 1; i < length_c; i++) begin
                assign register_s[i] = register_s[i-1];
            end
        end
    endgenerate

    always @(posedge shift_register_if.clock_i) begin
        if (shift_register_if.resetn_i == 0) begin
            register_s <= '{default: 0};
        end else begin
            register_s[0] = shift_register_if.data_i;
        end
    end
endmodule