//-------------------------------------------------------------------------------
//--Project: SystolicAccelerator
//--File: mac_array.sv
//--Date: 10.08.2022
//--Engineer : Ahmet Fatih ANKARALI
//--Description:
//-------------------------------------------------------------------------------
`include "systolic_pkg.svh"
import systolic_pkg::*;
interface mac_array_interface(
    
    input logic clock_i,               //system clock
    input logic resetn_i               //system active low reset;
    );

    t_mac_data a_rows_i [systolic_size_c];
    t_mac_data b_columns_i [systolic_size_c];
    t_mac_mul_data c_array_o [systolic_size_c][systolic_size_c];

    modport rtl(
        input  a_rows_i,
        input  b_columns_i,
        output  c_array_o
    );
endinterface
//TODO : Add delays to mac instantiations
module mac_array(mac_array_interface.rtl mac_array_if);

    mac_interface mac_if[systolic_size_c][systolic_size_c];

    genvar i,j, length_c;
    generate
        for(i=0; i<systolic_size_c; i++) begin
            for(j=0; i<systolic_size_c; i++) begin
                if ( (i == 0) && (j == 0) ) begin
                    assign length_c = 0;
                    assign mac_if[i][j].a_i = mac_array_if.a_rows_i[i];
                    assign mac_if[i][j].b_i = mac_array_if.b_columns_i[j];
                end else if (i == 0) begin
                    assign length_c = j;
                    assign mac_if[i][j].a_i = mac_if[i][j-1].x_o;
                    assign mac_if[i][j].b_i = mac_array_if.b_columns_i[j];
                end else if (j == 0) begin
                    assign length_c = i;
                    assign mac_if[i][j].a_i = mac_array_if.a_rows_i[i];
                    assign mac_if[i][j].b_i = mac_if[i-1][j].y_o;
                end else begin
                    assign length_c = 0;
                    assign mac_if.a_i[i][j] = mac_if[i][j-1].x_o;
                    assign mac_if.b_i[i][j] = mac_if[i-1][j].y_o;
                end
                mac #(.length_c(length_c) ) mac_inst(.mac_if(mac_if[i][j]));
            end
        end
    endgenerate

endmodule