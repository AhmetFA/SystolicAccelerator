//-------------------------------------------------------------------------------
//--Project: SystolicAccelerator
//--File: mac.sv
//--Date: 10.08.2022
//--Engineer : Ahmet Fatih ANKARALI
//--Description:
//-------------------------------------------------------------------------------
//TODO: Add input buffers
import systolic_pkg::*;
interface mac_interface(
    input logic clock_i,               //system clock
    input logic resetn_i               //system active low reset;
    );

    t_mac_data a_i;
    t_mac_data b_i;
    t_mac_data x_o;
    t_mac_data y_o;
    t_mac_mul_data c_o;

    modport rtl(
        input a_i,
        input b_i,
        output x_o,
        output y_o,
        output c_o
    );
endinterface

module mac #(parameter int length_c = 0) (mac_interface.rtl mac_if);
    generate
        if (length_c == 0) begin // No input delay case
            mul mul_inst(.mac_if(mac_if));
        end else begin // Delay on input data case
            shift_register_interface shift_reg_a_if;
            shift_register_interface shift_reg_b_if;
            mac_interface delayed_mac_if;

            assign shift_reg_a_if.data_i = mac_if.a_i;
            assign shift_reg_b_if.data_i = mac_if.b_i;

            shift_register #(.length_c(length_c) ) shift_reg_a_inst(.shift_register_if(shift_reg_a_if));
            shift_register #(.length_c(length_c) ) shift_reg_b_inst(.shift_register_if(shift_reg_b_if));

            assign delayed_mac_if.a_i = shift_reg_a_if.data_o;
            assign delayed_mac_if.b_i = shift_reg_b_if.data_o;

            mul delayed_mul_inst (.mac_if(delayed_mac_if));

            assign mac_if.x_o = delayed_mac_if.x_o;
            assign mac_if.y_o = delayed_mac_if.y_o;
            assign mac_if.c_o = delayed_mac_if.c_o;
        end
    endgenerate
endmodule

//Main part of MAC separately written to increase readibility
module mul (mac_interface.rtl mac_if);
    always @ (posedge mac_if.clock_i) begin
        if (mac_if.resetn_i == 0) begin
            mac_if.x_o <= 0;
            mac_if.y_o <= 0;
            mac_if.c_o <= 0;
        end else begin
            mac_if.x_o <= mac_if.a_i;
            mac_if.y_o <= mac_if.b_i;
            mac_if.c_o <= mac_if.a_i*mac_if.b_i;
        end
    end
endmodule